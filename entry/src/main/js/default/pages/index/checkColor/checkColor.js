import router from '@system.router';

export default {
    data(){
        return{
            colorType: "hex",
            colorValue: "#000000",
        }
    },
     getColorData(e){
        switch (this.colorType) {
            case "hex":
                this.colorValue=e.detail.colorValue
                break;
            case "rgba":
                this.colorValue= JSON.stringify(e.detail.colorValue)
                break;
        }
    },
    clickBack(){
        console.log('颜色'+this.colorValue)
        router.push({
            uri: 'pages/index/index',
            params:{
                colorValue: this.colorValue,
            }
        })
    },
}
