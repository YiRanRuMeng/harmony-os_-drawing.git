// @ts-nocheck
export default {
    props:{
        canvasWidth:{
            type:String,
            default: '240',
        },
        canvasHeight:{
            type:String,
            default: '240',
        },
        pointWidth:{
            type:String,
            default: '34',
        },
        pointHeight:{
            type:String,
            default: '34',
        },
        pointLeft:{
            type:String,
            default: '50%',
        },
        pointTop:{
            type:String,
            default: '50%',
        },
        colorType:{
            type:String,
            default:"rgba"   //rgba、hex
        },

    },
    data: {
        oTimer:null
    },

    onInit(){
        console.log('***************6668',this.colorDraw);
        clearTimeout(this.oTimer)
        var This = this
        this.oTimer=setTimeout(function(){
            This.drawGradient()
        },200)
    },
    onPageShow(){

    },

    // 绘图1
    drawGradient() {
        var iSectors = 360;
        var width=this.canvasWidth;
        var height=this.canvasHeight;
        var iSectorAngle = (360 / iSectors / 180) * Math.PI;
        var canvas = this.$refs.canvas;
        var ctx = canvas.getContext("2d");
        // 清除画布内容
        ctx.clearRect(0, 0, width, height);
        // 存储当前环境
        ctx.save();
        // 画布（0,0）初始点映射到中间圆点
        ctx.translate(width / 2, height / 2);

        for (let i = 0; i < 360; i++) {
            var startAngle = 0;
            var endAngle = startAngle + iSectorAngle;
            var radius = width / 2 - 1;
            // iSectorAngle * i  变量弧度的计算
            var grd = ctx.createLinearGradient(
                0,
                0,
                width / 4,
                height / 4
            );
            grd.addColorStop(0,`rgb(${this.hslToRgb(i / 360, 0, 1)})`);
            grd.addColorStop(1,`rgb(${this.hslToRgb(i / 360, 1, 0.5)})`);
            ctx.beginPath();
            ctx.moveTo(0, 0);
            // False = 顺时针，true = 逆时针
            ctx.arc(0, 0, radius, startAngle, endAngle, false);

            ctx.closePath();
            // 设置或返回用于笔触的颜色、渐变或模式。（边）
            ctx.strokeStyle = grd;

            // 绘制已定义的路径。
            ctx.stroke();
            // 设置或返回用于填充绘画的颜色、渐变或模式。（内容）
            ctx.fillStyle = grd;
            // 填充当前绘图（路径）。
            ctx.fill();
            // 都是在同一地方绘出的点，需要通过旋转来调整位置。
            // 重复360，调整360
            ctx.rotate(-iSectorAngle);
        }
        // 返回之前保存过的路径状态和属性。
        ctx.restore();
    },
//将rgb转换为hsl对象()
    rgbToHslObj(r, g, b) {
        r /= 255;
        g /= 255;
        b /= 255;
        var max = Math.max(r, g, b);
        var min = Math.min(r, g, b);
        var diff = max - min;
        var twoValue = max + min;
        var obj = {h:0, s:0, l:0};
        if(max == min) {
            obj.h = 0;
        } else if(max == r && g >= b) {
            obj.h = 60 * (g - b) / diff;
        } else if(max == r && g < b) {
            obj.h = 60 * (g - b) / diff + 360;
        } else if(max == g) {
            obj.h = 60 * (b - r) / diff + 120;
        } else if(max == b) {
            obj.h = 60 * (r - g) / diff + 240;
        }
        obj.l = twoValue / 2;
        if(obj.l == 0 || max == min) {
            obj.s = 0;
        } else if(0 < obj.l && obj.l <= 0.5) {
            obj.s = diff / twoValue;
            //obj.s = diff / (2 * obj.l);
        } else {
            obj.s = diff / (2 - twoValue);
            //obj.s = diff / (2 - 2 * obj.l);
        }
        obj.h = Math.round(obj.h);
        return obj;
    },

/**
     * HSL颜色值转换为RGB.
     * 换算公式改编自 http://en.wikipedia.org/wiki/HSL_color_space.
     * h, s, 和 l 设定在 [0, 1] 之间
     * 返回的 r, g, 和 b 在 [0, 255]之间
     *
     * @param   Number  h       色相
     * @param   Number  s       饱和度
     * @param   Number  l       亮度
     * @return  Array           RGB色值数值
     */
    hslToRgb(h, s, l) {
        var r, g, b;

        if(s == 0) {
            r = g = b = l; // achromatic
        } else {
            var hue2rgb = function hue2rgb(p, q, t) {
                if(t < 0) t += 1;
                if(t > 1) t -= 1;
                if(t < 1/6) return p + (q - p) * 6 * t;
                if(t < 1/2) return q;
                if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
                return p;
            }

            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1/3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1/3);
        }
        //        console.log([Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)])
        return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
    },
    //在canvas拖拽移动
    getCanvasTouchMove(e){
        var local=e.touches[0]
        this.limitArea(local, this.canvasWidth)
    },
    //点击canvas移动
    getCanvasTouchEnd(e){
        var local=e.touches[0]
        this.limitArea(local, this.canvasWidth)
    },
    //点击canvas移动
    getPointTouchMove(e){
        var local=e.touches[0]
        this.limitArea(local, this.canvasWidth)
    },
// 获取rgba颜色
    getRgbaAtPoint(pos) {
        var canvas = this.$refs.canvas;
        var ctx = canvas.getContext("2d");
        var imgData = ctx.getImageData(0, 0, this.canvasWidth, this.canvasHeight);
        var data = imgData.data;
        // getImageData数组的数量是canvas的宽度 * 高度 * 4 ，因为图片是rgba颜色组成的
        // 因为有rgba四个数值所以要对应*4，要获取四个数值对应一个点
        // 比如原先是x(1)对应一个y(1234)数组，但现在y数组对应一个点需要有对应4个参数(rgba),所以y就变成了(1234....16)
        // 现在x从y数组里需要获取到一个点，就需要*4，因为y数组4个参数代表一个点。
        // 索引值获取方式：纵坐标*图像宽度+横坐标 canvas的颜色点从(0,0)到（240,240）采用线性分布的方式
        var dataIndex = (pos.localY * imgData.width + pos.localX) * 4;
        var colorArr = [
                data[dataIndex],
                data[dataIndex + 1],
                data[dataIndex + 2],
                (data[dataIndex + 3] / 255).toFixed(2),
        ];
        return colorArr;
    },
// RGB16进制
    colorHex(str) {
        var hexcode = "#";
        for (var i = 0; i < 3; i++) {
            if (str[i] < 16) {
                str[i] = "0" + str[i].toString(16);
            } else {
                str[i] = str[i].toString(16);
            }
            hexcode += str[i];
        }
        return hexcode.toUpperCase();
    },
// 范围限定与颜色渲染
    limitArea(posValue, widthValue) {
        let x = posValue.localX - widthValue / 2;
        let y = posValue.localY - widthValue / 2;
        let r = widthValue / 2 - 1;
        let colorValue,colorHex
        if (x * x + y * y < r * r) {
            colorValue=this.getRgbaAtPoint(posValue)
            this.pointLeft = posValue.localX + "px";
            this.pointTop = posValue.localY + "px";
            this.$refs.point.style.backgroundColor =  "rgba(" + colorValue + ")";
            this.$refs.point.style.borderWidth = "2px";
            this.$refs.point.style.borderColor = "#ffffff";
//            colorHex=this.colorHex(colorRgba)
            this.selectColor(colorValue)

        } else {
            console.log("触点不在范围内!");
        }
    },
    selectColor(colorValue){
        switch (this.colorType) {
            case "hex":
                colorValue = this.colorHex(colorValue)
              break;
            case "rgba":
                colorValue = colorValue
              break;
        }
        this.$emit("getColor",{colorValue})
    }
}