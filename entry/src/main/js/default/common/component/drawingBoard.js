import router from '@system.router';
export default {
    data() {
        return {
            canvas: null,
            canvasTxt: null,
            points: [],
            beginPoint: null,
            lineWidth: 4, //默认笔画粗细
            isDraw: false, //签名标记
            signColor: '#ffffff',
            checkColor: "/common/images/color.png",
            toolList: [
                {
                    name: '画笔',
                    type: 1,
                    src: '/common/images/pen.png',
                },
                {
                    name: '橡皮檫',
                    type: 2,
                    src: '/common/images/reaser.png',
                },
                {
                    name: '清除',
                    type: 3,
                    src: '/common/images/clear.png',
                }, {
                    name: '保存',
                    type: 4,
                    src: '/common/images/save.png',
                },
            ],
            penType: 'source-over', //默认画笔状态
            colorOn: 0,
            sizeOn: 0,
            toolOn: 0,
            colorValue: '',
            colorType: "hex",
            isInitColor:"", // 是否渲染色盘组件
        }
    },
    props: {
        brushSizes: {
            type: Array,
            default: [3,8,16]
        },
        brushColor: {
            type: Array,
            default: []
        },

        // 取消
        public_cancel:{
            type:String,
            default:"取消"
        },
        // 确定
        public_sure:{
            type:String,
            default:"确定"
        },
    },
    onInit() {
    },
    showDialog() {
        router.push({
            uri: 'pages/index/checkColor/checkColor',
        })
    },
    cancelSchedule(e) {
        this.$element('simpledialog').close();

    },
    setSchedule(e) {
        this.$element('simpledialog').close();

    },

    onAttached(){
        let _this = this;
        setTimeout(() => {_this.initCanvas()})
    },
    setTool(item,index) {
        console.log(index);
        if(index == 0 || index == 1){
            this.toolOn = index;
        }
        let type = item.type;
        switch(type){
            case 1:
                console.log('点击画笔')
                this.penType = 'source-over';
                this.canvasTxt.lineWidth = this.lineWidth;
                this.canvasTxt.globalCompositeOperation = this.penType;
                break;
            case 2:
                console.log('点击橡皮檫')
                this.penType = 'destination-out';
                this.canvasTxt.lineWidth = this.lineWidth;
                this.canvasTxt.globalCompositeOperation = this.penType;
                break;
            case 3:
                this.overwrite();
                break;
            case 4:
                console.log('点击保存')
                this.savaCanvas();
                break;
        }
    },
    selectColor(color,index) {
        console.log(color,index);
        this.colorOn = index;
        console.log(color)
        this.signColor = color;
        console.log(this.canvasTxt)
        this.canvasTxt.fillStyle = this.signColor;
        this.canvasTxt.strokeStyle = this.signColor;
        this.canvasTxt.lineWidth = this.lineWidth;
    },
    selectSize(size,index) {
        this.sizeOn = index;
        this.lineWidth = size;
        this.canvasTxt.lineWidth = this.lineWidth;
    },
    initCanvas(){
        this.canvas = this.$refs.canvas;
        this.canvasTxt = this.canvas.getContext('2d',{ antialias: true });
        this.canvasTxt.strokeStyle = this.signColor;
        this.canvasTxt.lineJoin = "round";
        this.canvasTxt.lineCap = "round";
        this.canvasTxt.globalCompositeOperation = this.penType;
        this.canvasTxt.lineWidth = this.lineWidth;
    },
    drawTouchStart(ev) {
        this.initCanvas();
        const { x, y } = this.getPoints(ev.touches[0]);
        this.points.push({ x, y }, { x, y }, { x, y }, { x, y }); //因为需要至少三个点才能开始绘制线条
        this.beginPoint = { x, y };
    },
    drawTouchMove(ev) {
        const { x, y } = this.getPoints(ev.touches[0]);
        this.points.push({ x, y });
        // 通过二次贝塞尔曲线优化线条，将第2，第3的中点为第二点，第2为控制点
        if (this.points.length > 3) {
            this.isDraw = true; //签名标记, 设置在这里是为了防止用户只点一下并没有开始画线，点击保存得到空白图片
            const lastTwoPoints = this.points.slice(-2);
            const controlPoint = lastTwoPoints[0];
            const endPoint = {
                x: (lastTwoPoints[0].x + lastTwoPoints[1].x) / 2,
                y: (lastTwoPoints[0].y + lastTwoPoints[1].y) / 2,
            };
            this.drawLine(this.beginPoint, controlPoint, endPoint);
            // 将后一个点作为在一个画线点前一个点
            this.beginPoint = endPoint;

        }
    },
    drawTouchEnd() {
        if (this.points.length > 3) {
            const lastTwoPoints = this.points.slice(-2);
            const controlPoint = lastTwoPoints[0];
            const endPoint = lastTwoPoints[1];
            this.drawLine(this.beginPoint, controlPoint, endPoint);
        }
        this.beginPoint = null;
        this.points = [];
    },
    // 记录前一个点
    getPoints(res) {
        return {
            x: res.localX,
            y: res.localY,
        };
    },
// 开始画线
drawLine(beginPoint, controlPoint, endPoint) {
this.canvasTxt.beginPath();
this.canvasTxt.moveTo(beginPoint.x, beginPoint.y);
this.canvasTxt.quadraticCurveTo(
    controlPoint.x,
    controlPoint.y,
    endPoint.x,
    endPoint.y
);
this.canvasTxt.stroke();
this.canvasTxt.closePath();
},
    //抹除功能
    overwrite() {
        this.canvasTxt.clearRect(0, 0, 1920,1920);
        this.points = [];
        this.isDraw = false; //签名标记
    },
    // 保存功能
    savaCanvas(){
        var dataURL = this.$refs.canvas.toDataURL();
        console.log('图片base64信息'+dataURL);
        this.$emit('eventDataurl',dataURL)

    }

}
