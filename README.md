
# 基于鸿蒙FA开发的绘画板组件

### 10分钟教会你轻松实现Harmony OS手写板

> 随着科技的发达，在日常生活中我们逐渐的脱离的笔和纸，但往往还有许多场景我们还是需要涂涂画画，不论是开会或者设计等等刚好身边没有笔纸，我们的画板就排上用场了。我们还可以扩展将其作为和键盘鼠标一样的输入设备等等。还有更多的使用场景让我们一起探索。

### 功能介绍

画板组件是基于OpenHarmony下的JavaScript UI框架开发的一款组件，主要特点如下：

1. 支持画笔粗细选择
2. 支持画笔颜色定义选中
3. 画笔颜色除了默认颜色，可点击色盘，选择自己想要的颜色
4. 一件清除画板
5. 支持橡皮擦功能
6. 支持保存图片功能

<font color="#ff0000">注意：</font>
1. 保存功能需要api >= 6，得到的是一个base64的数据，可以根据自己的需要自行调整
2. 功能操作区域可左右滑动选择

 **效果展示**

 ![演示文件](./entry/src/main/resources/base/media/1.jpg) ![演示文件](./entry/src/main/resources/base/media/2.jpg) 
 
 **视频展示**
 ![演示文件](./entry/src/main/resources/base/media/demo.gif)



 

**代码模块说明**

```javascript
// 可以根据自己的实际情况，传入需要的画笔颜色和画笔大小
props: {
// 画笔粗细
brushSizes: {
    type: Array,
    default: [3,8,16]
},
// 画笔颜色
brushColor: {
    type: Array,
    default: ['#ffffff',"#000000","#ff9800",'#3f51b5','#ff5722',"#4caf50"]
}

// 工具设置
setTool(item,index) {
    console.log(index);
    if(index == 0 || index == 1){
        this.toolOn = index;
    }
    let type = item.type;
    switch(type){
        case 1:
            console.log('点击画笔')
            this.penType = 'source-over';
            this.canvasTxt.lineWidth = this.lineWidth;
            this.canvasTxt.globalCompositeOperation = this.penType;
            break;
        case 2:
            console.log('点击橡皮檫')
            this.penType = 'destination-out';
            this.canvasTxt.lineWidth = this.lineWidth;
            this.canvasTxt.globalCompositeOperation = this.penType;
            break;
        case 3:
            this.overwrite();
            break;
    }
},
// 画笔颜色选择
selectColor(color,index) {
    console.log(index);
    this.colorOn = index;
    console.log(color)
    this.signColor = color;
    console.log(this.canvasTxt)
    this.canvasTxt.fillStyle = this.signColor;
    this.canvasTxt.strokeStyle = this.signColor;
    this.canvasTxt.lineWidth = this.lineWidth;
},
// 画笔粗细设置
selectSize(size,index) {
        this.sizeOn = index;
        this.lineWidth = size;
        this.canvasTxt.lineWidth = this.lineWidth;
    },
// 开始画线
drawLine(beginPoint, controlPoint, endPoint) {
  this.canvasTxt.beginPath();
  this.canvasTxt.moveTo(beginPoint.x, beginPoint.y);
  this.canvasTxt.quadraticCurveTo(
      controlPoint.x,
      controlPoint.y,
      endPoint.x,
      endPoint.y
  );
  this.canvasTxt.stroke();
  this.canvasTxt.closePath();
},
//一键清除功能
overwrite() {
  this.canvasTxt.clearRect(0, 0, 1920,1920);
  this.points = [];
  this.isDraw = false; //签名标记
},
// 保存功能
savaCanvas(){
    var dataURL = this.$refs.canvas.toDataURL();
    console.log('图片base64信息'+dataURL);
    this.$emit('eventDataurl',dataURL)
}
```
**使用说明**

在需要的页面直接引入即可
```javascript
<element name="drawing-board" src="../../common/component/drawingBoard.hml"></element>
<drawing-board @event-dataurl="getUrl"></drawing-board>

// 获取图片信息,可以根据自己的图片组件需要，处理对应的base64 数据
getUrl(valueUrl){
    console.log('得到图片信息'+JSON.stringify(valueUrl)); // "data:image/png;base64,xxxxxxxx..."
}
```
